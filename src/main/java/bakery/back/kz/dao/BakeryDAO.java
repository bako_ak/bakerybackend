package bakery.back.kz.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by baglanserikuly on 14.11.16.
 */
public interface BakeryDAO {

    @SqlUpdate("insert into shop (id, name, info,lat,lng) values (:id, :name, :info, :lat, :lng)")
    void insert(@Bind("id") int id, @Bind("name") String name, @Bind("info") String info, @Bind("lat") String lat, @Bind("lng") String lng);

    @SqlQuery("select name from shop where id = :id")
    String findNameById(@Bind("id") int id);

    void close();
}
